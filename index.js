/*

5. Import the http module using the required directive.
6. Create a variable port and assign it with the value of 3000.
7. Create a server using the createServer method that will listen in
to the port provided above.
8. Console log in the terminal a message when the server is
successfully running.
9. Create a condition that when the login route is accessed, it will
print a message to the user that they are in the login page.

*/

const http = require("http")
const port = 3000;

const server = http
  .createServer((req, res) => {
    if (req.url == "/login") {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("Welcome to the login page!");
    }

    else {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("I'm sorry, the page you are looking for cannot be found.");
    }

})
  .listen(port);

console.log(`Server running at localhost: ${port}`);